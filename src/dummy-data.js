const dummyData = [
  {
    id: 64,
    name: "xeno_camel_dark2569",
    date_created: 1454525670,
    salty_rank: 1.03,
    salty_comments: "134",
    comments_total: "16"
  },
  {
    id: 46,
    name: "xx_flower_cracker3954",
    date_created: 1355682472,
    salty_rank: 1.05,
    salty_comments: "112",
    comments_total: "118"
  },
  {
    id: 18,
    name: "cable_billie_jedi103",
    date_created: 1254064642,
    salty_rank: 1.39,
    salty_comments: "152",
    comments_total: "472"
  },
  {
    id: 12,
    name: "dame_greatness_dark3672",
    date_created: 1195088391,
    salty_rank: 1.3,
    salty_comments: "122",
    comments_total: "305"
  }
];

export default dummyData;