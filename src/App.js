import React from "react";
import Title from "./components/Title";
import {Switch, Route} from "react-router-dom";
import Home from "./components/Home";
import About from "./components/About";
import Login from "./components/Login";
import Register from "./components/Register";
import SetUsers from "./components/UsersContainer/SetUsers"

function App() {
  return (
    <div className="App">
      <Title />
        <Switch>
          <Route path={"/"}><Home /></Route>
          <Route path={"/about"}><About /></Route>
          <Route path={"/login"}><Login /></Route>
          <Route path={"/register"}><Register /></Route>
        </Switch>
      <SetUsers />
    </div>
  );
}

export default App;
