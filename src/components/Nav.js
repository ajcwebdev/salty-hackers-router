import React from "react";
import "./index.css";
import { Link } from "react-router-dom";

function Nav() {
  return (
    <nav>
      <div className="links">
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/login">Login</Link>
        <Link to="/register">Register</Link>
      </div>
    </nav>
  );
}

export default Nav;
