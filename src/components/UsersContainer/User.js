import React from "react";
import UserHeader from "./UserHeader";
import "./Users.css";

const User = props => {
  return (
    <div className="user-border">
      <UserHeader name={props.user.name} />
      <p>Data Created: {props.user.date_created}</p>
      <p>Total Comments: {props.user.comments_total}</p>
      <p>Salty Rank: {props.user.salty_rank}</p>
      <p>Salty Comments: {props.user.salty_comments}</p>
    </div>
  );
};

export default User;