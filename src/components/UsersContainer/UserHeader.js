import React from "react";
import "./Users.css";

const UserHeader = props => {
  return (
    <div className="user-header">
      <h2>{props.name}</h2>
    </div>
  );
};

export default UserHeader;