import React, { useState } from "react";
import User from "./User";
import "./Users.css";
import dummyData from "../../dummy-data.js";

const SetUsers = () => {

  const [users, setUsers] = useState(dummyData);

  return (
    <div className="users-container">
      {users.map((user, index) => (
        <User user={user} key={index} />
      ))}
    </div>
  );
};

export default SetUsers; 