import React from "react";
import styled from "styled-components";

const TitleStyle = styled.h1`
  color: #d610d6;
  text-align: center;
  border: 0.7rem solid powderblue;
  border-bottom: groove;
  font-weight: bolder;
`;

function Title() {
  return <TitleStyle>Hacker Troll News</TitleStyle>;
}

export default Title;
